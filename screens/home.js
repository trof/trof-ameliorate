import React from 'react';
import { StyleSheet, View, Text, ScrollView} from 'react-native';
import { Appbar } from 'react-native-paper';
import { DefaultTheme, Provider as PaperProvider, Switch, Card, Title, Paragraph, Button, Avatar, BottomNavigation } from 'react-native-paper';

const theme = {
  ...DefaultTheme,
  roundness: 2,
  colors: {
    ...DefaultTheme.colors,
    primary: '#20c063',
  },
};

export default function Home( {navigation} ) {

  const _goBack = () => console.log('Went back');

   const _handleSearch = () => console.log('Searching');

   const _handleMore = () => console.log('Shown more');

   const LeftContent = props => <Avatar.Icon {...props} icon="pin" />

   const [isSwitchOn, setIsSwitchOn] = React.useState(false);

     const onToggleSwitch = () => setIsSwitchOn(!isSwitchOn);

   const HomeRoute = () =>
   <ScrollView style={{flex:1}}>

         <Card>
           <Card.Title title="Card Title" subtitle="Card Subtitle" left={LeftContent} />
           <Card.Content>
             <Title>Card title</Title>
             <Paragraph>Card content</Paragraph>
           </Card.Content>
           <Card.Cover source={{ uri: 'https://images.unsplash.com/photo-1504674900247-0877df9cc836?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=900&q=60' }} />
           <Card.Actions>
             <Button>Cancel</Button>
             <Button>Ok</Button>
           </Card.Actions>
         </Card>
         <Card>
           <Card.Title title="Card Title" subtitle="Card Subtitle" left={LeftContent} />
           <Card.Content>
             <Title>Card title</Title>
             <Paragraph>Card content</Paragraph>
           </Card.Content>
           <Card.Cover source={{ uri: 'https://images.unsplash.com/photo-1485182708500-e8f1f318ba72?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=900&q=60' }} />
           <Card.Actions>
             <Button>Cancel</Button>
             <Button>Ok</Button>
           </Card.Actions>
         </Card>
     </ScrollView>;

  const ProfileRoute = () => <ScrollView><Text style={{margin:10}}>Profile</Text>
    <Switch style={{margin:10}} value={isSwitchOn} onValueChange={onToggleSwitch} /></ScrollView>
  ;

  const SettingsRoute = () => <Text style={{margin:10}}>Settings</Text>;

    const [index, setIndex] = React.useState(0);
    const [routes] = React.useState([
      { key: 'home', title: 'Home', icon: 'home'},
      { key: 'profile', title: 'Profile', icon: 'account'},
      { key: 'settings', title: 'Settings', icon: 'cog'},
    ]);

    const renderScene = BottomNavigation.SceneMap({
      home: HomeRoute,
      profile: ProfileRoute,
      settings: SettingsRoute
    });

  return (
    <PaperProvider theme={theme}>
    <View style={{flex: 1}}>
      <BottomNavigation
      navigationState={{ index, routes }}
      onIndexChange={setIndex}
      renderScene={renderScene}
    />
    </View>
    </PaperProvider>
  );
}
