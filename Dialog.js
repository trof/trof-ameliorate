import * as React from 'react';
import { View } from 'react-native';
import { Button, Paragraph, Dialog, Portal } from 'react-native-paper';

export default function DialogComponent({ }) {
  const [visible, setVisible] = React.useState(false);
  const showDialog = () => setVisible(true);
  const hideDialog = () => setVisible(false);
  return (
    <View>
      <Button onPress={showDialog}>"test"</Button>
      <Portal>
        <Dialog visible={visible} onDismiss={hideDialog}>
          <Dialog.Title>"test"</Dialog.Title>
          <Dialog.Content>
            <Paragraph>"test"</Paragraph>
          </Dialog.Content>
          <Dialog.Actions>
            <Button onPress={hideDialog}>"test"</Button>
          </Dialog.Actions>
        </Dialog>
      </Portal>
    </View>
  );
}
