import 'react-native-gesture-handler';
import React from 'react';
//import { Button, View, Text } from 'react-native';
//import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';
import Home from '../screens/home';
import Profile from '../screens/profile';
import Settings from '../screens/settings';
//import style from './App/style/styles';
//import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
//import DialogComponent from './Dialog.js';


const screens = {
  Home: {
    screen: Home
  },
  Profile : {
    screen: Profile
  },
  Settings : {
    screen: Settings
  }
}

const Stack = createStackNavigator(screens);

/*function HomeScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Home Screen</Text>
	  <Button
        title="Go to Profile"
        onPress={() => navigation.navigate('Profile')}
      />
    </View>
  );
}

function ProfileScreen() {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Profile Screen</Text>
    </View>
  );
}*/

export default createAppContainer(Stack);
