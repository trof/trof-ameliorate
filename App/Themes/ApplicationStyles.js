import { StyleSheet } from 'react-native'
import { totalSize, height, width } from 'react-native-dimension'
import colors from './Colors'
import family from './Fonts'
export default StyleSheet.create({
  bgContainer: {
    flex: 1,
    height: null,
    width: null
  },
  mainContainer: {
    flex: 1,
    backgroundColor: colors.appBgColor1
  },
  h1: {
    fontSize: totalSize(5),
    color: colors.appTextColor1,
    fontFamily: family.appTextBold
  },
  h2: {
    fontSize: totalSize(4),
    color: colors.appTextColor1,
    fontFamily: family.appTextMedium
  },
  h3: {
    fontSize: totalSize(3),
    color: colors.appTextColor1,
    fontFamily: family.appTextMedium
  },
  h4: {
    fontSize: totalSize(2),
    color: colors.appTextColor1,
    fontFamily: family.appTextMedium
  },
  h5: {
    fontSize: totalSize(1.5),
    color: colors.appTextColor1,
    fontFamily: family.appTextRegular
  },
  h6: {
    fontSize: totalSize(1.25),
    color: colors.appTextColor1,
    fontFamily: family.appTextRegular
  },
  inputContainerUnderLined: {
    marginHorizontal: width(5),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomWidth: 0.5,
    borderBottomColor: '#FFFF'
  },
  inputContainerBorderd: {
    marginHorizontal: width(5),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    borderWidth: 0.5,
    borderColor: colors.appColor1
  },
  inputContainerColored: {
    marginHorizontal: width(5),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    backgroundColor: '#FFFF',
    borderRadius: 2.5
  },
  inputField: {
    height: height(7),
    width: width(80),
    color: colors.appTextColor1,
    fontFamily: family.appTextLight,
    fontSize: totalSize(1.75)
  },
  inputFieldBorderd: {
    marginHorizontal: width(5),
    height: height(7),
    borderWidth: 0.5,
    borderColor: colors.appColor1,
    fontSize: totalSize(1.75),
    fontFamily: family.appTextRegular,
    borderRadius: 2.5
  },
  inputFieldUnderLined: {
    marginHorizontal: width(5),
    height: height(7),
    borderBottomWidth: 1,
    borderBottomColor: 'grey',
    fontSize: totalSize(1.75),
    fontFamily: family.appTextRegular,
  },
  inputFieldColored: {
    marginHorizontal: width(5),
    height: height(7),
    fontSize: totalSize(1.5),
    elevation: 5,
    paddingLeft: 5,
    backgroundColor: '#FFFF',
    borderRadius: 5
  },

  buttonBorderd: {
    marginHorizontal: width(5),
    height: height(8),
    borderRadius: 2.5,
    borderWidth: 1,
    borderColor: colors.appColor1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonColord: {
    marginHorizontal: width(5),
    height: height(8),
    borderRadius: 5,
    backgroundColor: colors.appColor1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonBorderdColord: {
    marginHorizontal: width(5),
    height: height(8),
    borderRadius: 100,
    borderWidth: 0.5,
    backgroundColor: '#FFFF',
    elevation: 5,
    borderColor: colors.appColor1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  SocialButtonColord: {
    height: height(8),
    marginHorizontal: width(5),
    borderRadius: 2.5,
    backgroundColor: colors.facebook,
    //  alignItems: 'center',
    //  justifyContent: 'center'
  },
  buttonText: {
    fontSize: totalSize(2),
    color: colors.appColor1,
    fontFamily: family.appTextMedium
  },
  compContainer: {
    marginHorizontal: width(5),
    marginVertical: height(2.5)
  },
  rowCompContainer: {
    marginHorizontal: width(5),
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: height(2.5)
  },
  headerTitle: {
    fontSize: totalSize(2),
    color: colors.appTextColor1,
    fontFamily: family.appTextBold,
    marginHorizontal: width(5)
  },
})