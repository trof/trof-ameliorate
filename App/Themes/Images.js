// leave off @2x/@3x
const images = {
    logo: require('../Assets/Logo/Trof_Logo.png'),
    user: require('../Assets/Images/User4.jpg'),
    user2: require('../Assets/Images/User3.jpg'),
    user3: require('../Assets/Images/User5.jpg'),
    onboarding_01: require('../Assets/Images/onboarding_01.png'),
    onboarding_02: require('../Assets/Images/onboarding_02.png'),
    onboarding_03: require('../Assets/Images/onboarding_03.png'),
    event_1: require('../Assets/Images/event-1.jpg'),
    event_2: require('../Assets/Images/event-2.jpg'),
    event_3: require('../Assets/Images/event-3.jpg'),
    event_4: require('../Assets/Images/event-4.jpg'),
    event_5: require('../Assets/Images/event-5.jpg')
}

export default images;
